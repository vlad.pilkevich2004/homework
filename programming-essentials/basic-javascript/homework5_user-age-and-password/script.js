/*Экранирование, это использование специальных символов как обычных при помощи "\".
"\" - это знак экранирования. Он служит для коректного прочтения браузером строки и не записывается туда после её прочтения. Чтобы поместить "\" в строку, можно написать "\\". Используется для записи некоторых спец. символов.*/


let firstName = prompt('What is your first name?', 'Vlad');
let lastName = prompt('What is your last name?', 'Pilkevich');
let birthday = prompt('Enter your birthday date, please.', '12.02.2004')
let createNewUser = function(firstName,lastName, birthday) {
    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
	    	return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
	    },
        getAge() {
            let nowDate = new Date();
            let userBirthday = new Date(birthday);
            this.age = Math.round((nowDate - userBirthday) / (24 * 3600 * 365 * 1000) - 1);
            return this.age;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4)
            // if (birthday.length == 10) {
            //     return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6));
            // } if (birthday.length == 9) {             
            //     return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(5));
            // } else {
            //     return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(4));
            // }
        },    
    };   
    return newUser;
}
console.log(createNewUser(firstName, lastName, birthday).getLogin());
console.log(createNewUser(firstName, lastName, birthday).getAge());
console.log(createNewUser(firstName, lastName, birthday).getPassword());

