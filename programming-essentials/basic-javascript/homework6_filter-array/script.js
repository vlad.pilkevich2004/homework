/*
forEach это метод перебора масива. Он делает код меньше и легче для прочтения. Для перебора масива лучше использовать forEach, нежели for in, потому что он не он не оьбрабатывает все перечесляемые свойства масива. В forEach мы передеём функцию и она выполняется по разу для каждого елемента масива, какие являются аргументами данной функции. Элементы с неприсвоеными значениямим не обрабатываются.
*/

let arr = ['hello', 'world', 23, '23', null, true, false, , undefined, {}, []];
console.log(arr);
let mes = prompt(`string (no 'hello', 'world', '23') \n number (no 23) \n object (no null, {}, []) \n boolean (no true, false) \n undefined (no , , undefined)`);
let filterBy = (array, message) => { const newArray = array.filter(let = (elem) => { return (typeof elem) !== message;})    
console.log(newArray);
}; 
filterBy(arr, mes);




//Тоже самое, что и на строчках 8-11, но не стрелочным методом.

// function filterBy(array, message) {
//     const newArray = array.filter(function(elem){
//         return (typeof elem) !== message;
//     })    
//     console.log(newArray);
// };  
// filterBy(arr, mes);
