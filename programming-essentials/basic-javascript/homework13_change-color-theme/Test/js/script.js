window.onload = function () {
    if (localStorage.getItem('theme') !== null) {
        if (document.getElementById('page').classList.contains('page-dark')) {
            document.getElementById('page').classList.remove('page-dark')
            document.getElementById('title').classList.remove('title-dark')
            document.getElementById('text').classList.remove('text-dark')
        } else if (document.getElementById('page').classList.contains('page')) {       
            document.getElementById('page').classList.add('page-dark')
            document.getElementById('title').classList.add('title-dark')
            document.getElementById('text').classList.add('text-dark')
        }
    }
    let color = document.querySelector('#theme')
    color.addEventListener('click', function () {
        if (document.getElementById('page').classList.contains('page-dark')) {
            document.getElementById('page').classList.remove('page-dark')
            document.getElementById('title').classList.remove('title-dark')
            document.getElementById('text').classList.remove('text-dark')
            localStorage.removeItem('theme')
        } else {       
            document.getElementById('page').classList.add('page-dark')
            document.getElementById('title').classList.add('title-dark')
            document.getElementById('text').classList.add('text-dark')
            localStorage.setItem('theme', 'dark')
        }
    })    
}


// window.onload = function () {
//     if (localStorage.getItem('theme') !== null) {
//         name()
//     }
//     let color = document.querySelector('#theme')
//     color.addEventListener('click', function () {
//         name()
//     })    
// }

// function name() {
//     if (document.getElementById('page').classList.contains('page-dark')) {
//         document.getElementById('page').classList.remove('page-dark')
//         document.getElementById('title').classList.remove('title-dark')
//         document.getElementById('text').classList.remove('text-dark')
//         localStorage.removeItem('theme')
//     } else {       
//         document.getElementById('page').classList.add('page-dark')
//         document.getElementById('title').classList.add('title-dark')
//         document.getElementById('text').classList.add('text-dark')
//         localStorage.setItem('theme', 'dark')
//     }
// }
