window.onload = function () {
    if (localStorage.getItem('theme') !== null) {
        locStor()
    }
    let color = document.querySelector('#theme')
    color.addEventListener('click', function() {
        locStor()
    })    
}
function locStor() {
    if (document.getElementById('page').classList.contains('page-dark')) {
        document.getElementById('page').classList.remove('page-dark')
        document.getElementById('a').classList.remove('a-dark')
        document.getElementById('frame').classList.remove('frame-dark')
        document.getElementById('menu-left').classList.remove('menu-left-dark')
        document.querySelectorAll('.text').forEach(e => e.classList.remove('text-dark'))
        document.querySelectorAll('.menu-tlink').forEach(e => e.classList.remove('menu-tlink-dark'))
        localStorage.removeItem('theme')
    } else if (document.getElementById('page').classList.contains('page')) {       
        document.getElementById('page').classList.add('page-dark')
        document.getElementById('a').classList.add('a-dark')
        document.getElementById('frame').classList.add('frame-dark')
        document.getElementById('menu-left').classList.add('menu-left-dark')
        document.querySelectorAll('.text').forEach(e => e.classList.add('text-dark'))
        document.querySelectorAll('.menu-tlink').forEach(e => e.classList.add('menu-tlink-dark'))
        localStorage.setItem('theme', 'dark')
    }
}
