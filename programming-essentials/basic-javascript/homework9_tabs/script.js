const tabs = document.querySelector('.tabs')
const text = document.querySelectorAll('.tabs-content li')
tabs.addEventListener('click', function (event) {
    //console.log(event.target)
    event.target.closest('ul').querySelector('.active').classList.remove('active') 
    event.target.classList.add('active') 
    const target = event.target.dataset.tabName
    //console.log(target)
    text.forEach(item => {
        item.setAttribute('hidden', true)
        if (target === item.dataset.tabText) {
            item.removeAttribute('hidden')
        }
    })
})
