let input = document.getElementById("input1")
let eye = document.getElementById('icon')
let eye0 = document.getElementById('no-icon')
eye.addEventListener('click', function () {
    input.type = 'text';
    eye.replaceWith(eye0)
    eye0.style.display = 'inline'       
})
eye0.addEventListener('click', function () {        
    input.type = 'password'
    eye0.replaceWith(eye)
    eye.style.display = 'inline'
})
//---------------------------------------------------------------------------------------------------------------------//
let input1 = document.getElementById("input2")
let eye1 = document.getElementById('icon1')
let eye2 = document.getElementById('no-icon1')
eye1.addEventListener('click', function () {
    input1.type = 'text';
    eye1.replaceWith(eye2)
    eye2.style.display = 'inline'       
})
eye2.addEventListener('click', function () {        
    input1.type = 'password'
    eye2.replaceWith(eye1)
    eye1.style.display = 'inline'
})
//---------------------------------------------------------------------------------------------------------------------//
let send = document.querySelector('#button')
send.addEventListener('click', function (event) {
    event.preventDefault()
    if (input.value === input1.value) {
        alert('You are welcome!')
    } else {
        let no = document.createElement('span')
        document.body.append(no)
        no.innerText = 'Нужно ввести одинаковые значения'
        no.style.color = 'red'
        send.addEventListener('click', () => no.remove())
    }
})
