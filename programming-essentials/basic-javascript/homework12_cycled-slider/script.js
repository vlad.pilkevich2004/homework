/*
1.setTimeout вызывает функцию один раз через какое-то время. А setInterval вызывает функцию повторно снова и снова через определённое время.
2.Если в setTimeout передать нулевую задержку то она сработает мгновенно . Потому что это будет что-то типо вызова функции.
3.Если setInterval был вызван и больше не нужен, то его стоит сразу убирать (clearInterval()), чтобы не загружать сайт и не делать ненужные действия. А если нам нужен просто таймер и одно действие, то можно использовать setTimeout.
*/

const img = document.querySelectorAll('.image-to-show');
let div = document.querySelector('#btns')
let timerId;
let index = 0;
makeInterval()
function  makeInterval() {
    timerId = setInterval(() => {
        img[index].style.display = 'none'
        index++;
        if (index === img.length) {
            index = 0
        }
        img[index].style.display = 'block'
    }, 3000);    
}
div.addEventListener('click', function (event) {
    if (event.target.id === 'stop') {
        clearInterval(timerId)
    } else if (event.target.id === 'continue') {
        makeInterval()
    }
})
