
function calculator() {
  const firstOperand = +prompt('Enter operand');
  const operator = prompt('Enter operator');
  const secondOperand = +prompt('Enter operand');
  switch (operator) {    
    case "+":
      return firstOperand + secondOperand;
    case "-":
      return firstOperand - secondOperand;
    case "/":
      return firstOperand / secondOperand;
    case "*":
      return firstOperand * secondOperand;
    default:
      alert('Enter one of next signs: "+" "-" "*" "/"');
  } 
}
console.log(calculator());
