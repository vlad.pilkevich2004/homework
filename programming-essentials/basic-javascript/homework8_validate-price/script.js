/* Обработчик событий, это действие, которое может совершить user на сайте. Нажать на кнопку, ввести что-то в input, навести мышку на какой-то элемент, нажать какую то кнопку на клавиатуре. И мы с помощью обработчиков событий добавляем этим простым действия какого-то контента и удобного функционала. Тоесть когда происходит какое то событие, браузер вызывает обработчик.*/



let input = document.querySelector('#input');

input.addEventListener('focus', function () {
    input.style.border = '4px solid green'; 
})

let loseFocus = function () {
    input.style.border = null;
    document.body.onsubmit = (function() {
        let message = document.querySelector('#input').value
        if (message < 0) {
            input.style.border = '2px solid red';
            let no = document.createElement('span')
            document.body.append(no)
            no.innerText = 'Please enter correct price.'
            no.style.color = 'red'
            input.addEventListener('focus', () => {
                no.remove()
                document.querySelector('#input').value=''
            })
        } else {
            let div = document.createElement('div')
            document.body.prepend(div)
            div.classList.add('div')
            let span = document.createElement('span')
            div.prepend(span)
            span.innerText = `Текущая цена: ${message}`
            console.log(message)
            span.style.color = 'green'
            span.classList.add('spanTrue')
            let x = document.createElement('button')
            div.append(x)
            x.innerText = 'X'
            x.classList.add('x') 
            x.addEventListener('click', function () {
                span.remove()
                x.remove()
                document.querySelector('#input').value=''
                div.remove()
            })
        }
    }());  
}

