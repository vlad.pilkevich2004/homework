console.log('Task 1');

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const bothClients = [...clients1, ...clients2];
// console.log(bothClients);

const bothClientsFiltered = [...new Set(bothClients)]

console.log(bothClientsFiltered);

console.log('-------------------------------------');