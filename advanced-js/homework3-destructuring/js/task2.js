console.log('Task 2');

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

// let charactersShortInfo = characters.reduce((acc, item) => {
//     function newObjCrete() {
//         let { name, lastName, age } = item
//         return item = { name, lastName, age }
//     }
//     newObjCrete()
//     // console.log(item);
//     acc.push(item)
//     return acc
// }, [])

let charactersShortInfo = characters.map(({name, lastName, age}) => ({name, lastName, age}))

console.log(charactersShortInfo);


console.log('-------------------------------------');