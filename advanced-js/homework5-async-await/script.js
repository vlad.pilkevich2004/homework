/*
Асинхронная функция, это функция, которая имеет отдельное место в стеке вызовов. Она возвращает промис. Чтобы показать, что функция асинфхронная, мы перед ней пишем async.
*/

// ____________ Вариант 1 ________________

const buttonToFindIP = document.getElementById('IP');
const divOnPage = document.getElementById("block");

buttonToFindIP.addEventListener('click', function(){
    console.log('click');
    render();
})

const findUserIP = async function () {
    const userIP = await fetch("https://api.ipify.org/?format=json", {method: "GET"})
        .then(response => response.json());
    return userIP.ip;
}


const findUserInformationByIP = async function () {
    const userIP = await findUserIP().then(result => result);
    const throwUserIP = await fetch(`http://ip-api.com/json/${userIP}?fields=continent,country,regionName,city,district,zip`)
        .then(response => response.json())
    return throwUserIP;
}

const render = async function() {
    const userInformation = await findUserInformationByIP().then(result => result);
    console.log(userInformation);
    const {continent, country, regionName, city} = userInformation;
    divOnPage.innerHTML = "";
    divOnPage.innerHTML = `
        <p id="continent">Continent: ${continent}</p>
        <p id="country">Country: ${country}</p>
        <p id="regionName">Region Name: ${regionName}</p>
        <p id="city">City: ${city}</p>
    `;
    // divOnPage.insertAdjacentHTML('beforeend', `
    //     <p id="continent">Continent: ${continent}</p>
    //     <p id="country">Country: ${country}</p>
    //     <p id="regionName">Region Name: ${regionName}</p>
    //     <p id="city">City: ${city}</p>
    // `);
}