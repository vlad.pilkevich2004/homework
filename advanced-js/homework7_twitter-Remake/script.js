const BASE_URL = "https://ajax.test-danit.com/api/json";
const createPostButton = document.getElementById("createPost");
const buttonSub = document.getElementById("submitCreate");
const modalWin = document.getElementById("modalWin");

class API {
    getRequest(url) {
        return fetch(`${url}`, { method: 'GET' })
            .then(response => {
                if (response.ok) {
                    return response.json()
                }
                throw new Error(response.status)
            })
            .catch(err => { throw new Error(err) })
    }
    deleteRequest(id, url, countButtons, count) {
        [...document.getElementsByClassName('title')].forEach(elem => { if (elem.getAttribute("buttonid") === id) { elem.remove() } });
        [...document.getElementsByClassName('post-text')].forEach(elem => { if (elem.getAttribute("buttonid") === id) { elem.remove() } });
        [...document.getElementsByClassName('button-to-remove')].forEach(elem => { if (elem.getAttribute("buttonid") === id) { elem.remove() } });
        fetch(`${url}/${id}`, { method: 'DELETE' })
            .then(response => console.log(response))
        if (countButtons.length === 1) {
            // console.log(countButtons);
            // console.log(count);
            document.getElementById(count).remove();
        }
    }
    postRequest(name, body, title, email) {
        console.log(name, body, title, email);
        fetch(`${BASE_URL}/posts`, {
            method: 'POST',
            body: JSON.stringify({
                name: name,
                body: body,
                title: title,
                email: email
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(json => console.log(json))
        createNewPostRender(name, body, title, email);
    }
}
const api = new API()
class RenderUsers {
    constructor() {
        this.posts = document.getElementById('posts')
        this.users = api.getRequest(`${BASE_URL}/users`)
        this.wrapper = document.createElement("div")
    }
    render() {
        // console.log(this.users)
        this.users.then(response => {
            response.map(element => {
                let { name, username, email, id } = element;
                // console.log(name, id);
                this.wrapper.insertAdjacentHTML("beforeend", `
                    <div id="${id}" class="post">
                        <p class="username">Username: ${username}</p>
                        <p class="email">Email: ${email}</p>
                        <p class="full-name">Name: ${name}</p>
                    </div>
                    `)
                this.posts.append(this.wrapper)
            });
        })
    }
}
const user = new RenderUsers()
user.render()
class RenderPosts {
    constructor() {
        this.posts = api.getRequest(`${BASE_URL}/posts`)
        this.wrapper = document.createElement("div")
    }
    render() {
        this.posts.then(response => {
            response.forEach(element => {
                const div = document.getElementById(element.userId);
                const button = document.createElement('button')
                button.setAttribute("buttonId", element.id)
                button.setAttribute("countItem", element.userId)
                button.classList.add('button-to-remove')
                div.appendChild(button)
                button.innerText = 'DELETE'
                button.addEventListener('click', this.deletePost.bind(this))
                div.insertAdjacentHTML("beforeend", `
                        <h2 buttonid="${element.id}" class="title">Title: ${element.title}</h2>
                        <p buttonid="${element.id}" class="post-text">Post text: ${element.body}</p>
                    </div>
                    `)
            });
        })
    }

    deletePost(event) {
        api.deleteRequest(event.target.getAttribute("buttonid"), `https://ajax.test-danit.com/api/json/posts`, document.querySelectorAll(`[countItem='${event.target.getAttribute("countItem")}']`), event.target.getAttribute("countItem"))
    }
}
const posts = new RenderPosts()
posts.render()
class CreateNewPost {
    render() {
        modalWin.classList.remove("modal-new");
    }
    readInformation() {
        // console.log(document.getElementById("form"));
        // document.querySelectorAll("#form [type='name']").forEach(i=>console.log(i));
        // console.log(document.querySelectorAll("#form [type='name']")[0].value);
        let name = document.querySelectorAll("#form [type='name']")[0].value;
        let email = document.querySelectorAll("#form [type='email']")[0].value;
        let title = document.querySelectorAll("#form [type='title']")[0].value;
        let text = document.querySelectorAll("#form [type='text']")[0].value;
        // console.log(name, email, title, text);
        api.postRequest(name, email, title, text);
    }
}
const newPost = new CreateNewPost()
createPostButton.addEventListener('click', function () {
    newPost.render();
})
buttonSub.addEventListener('click', function () {
    modalWin.classList.add("modal-new");
    newPost.readInformation();
})

function createNewPostRender(name, body, title, email) {
    document.getElementById('posts').insertAdjacentHTML('afterbegin', `

    <div class="post">
        <p class="email">Email: ${email}</p>
        <p class="full-name">Name: ${name}</p>
        <h2 class="title">Title: ${title}</h2>
        <p class="post-text">Post text: ${body}</p>
    </div>
    `)
}