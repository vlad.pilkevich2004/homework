// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

/*
    Когда у нас происходит ошибка, то работа всего кода останавливается (весь код падает). Для этого мы используем try ... catch, то есть в момент ошибки выполение try прекращается и начинает выполняться catch. Мы можем это использовать, например, когда у нас проблема с сервером и загрузкой страницы (я имею ввиду про что-то типо ошибки 404) и в таких ситуация мы должны вывести пользователю модалальное окно с сообщением, что у нас проблемы с сервером (может что-то в беке слечилось) и чтобы он попробовал перезайти позднее.
*/

const books = [
    { 
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70 
    }, 
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    }, 
    { 
        name: "Тысячекратная мысль",
        price: 70
    }, 
    { 
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    }, 
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }   
];

let booksOnPage = books.reduce((acc, item, index) => {
    if (item.author != undefined && item.name != undefined && item.price != undefined) {
        acc.push(item)
    } else {
        try {
            // if ... else
            // if (!item.name) {
            //     throw new Error(`Данные неполны в книге под номером ${index + 1}: нет имени`);
            // } else if (!item.author) {
            //     throw new Error(`Данные неполны в книге под номером ${index + 1}: нет автора`);
            // } else if (!item.price) {
            //     throw new Error(`Данные неполны в книге под номером ${index + 1}: нет цены`);
            // } else {
            //     throw new Error("Что-то другое");
            // }

            // switch
            let a;
            switch (!a) {
                case !item.name:
                    throw new Error(`Данные неполны в книге под номером ${index + 1}: нет имени`);
                case !item.author:
                    throw new Error(`Данные неполны в книге под номером ${index + 1}: нет автора`);
                case !item.price:
                    throw new Error(`Данные неполны в книге под номером ${index + 1}: нет цены`);
                default:
                    throw new Error("Что-то другое");
            }
        } catch (err) {
            console.error(err);
        }
    }
    return acc;
}, [])
console.log(booksOnPage);

const divOnPage = document.getElementById('root');

let ul = document.createElement('ul')
divOnPage.prepend(ul)
ul.id = 'ulBooksList'

function bookList (array) {
    for (let i = 0; i < array.length; i++) {
        let li = document.createElement('li')
        ul.prepend(li)
        li.innerText = (`Название книги: ${array[i].name}. Автор: ${array[i].author}. Цена: ${array[i].price}`)
	}
}
bookList(booksOnPage)