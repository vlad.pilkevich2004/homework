/*
Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
AJAX - это асинхронный JS. То есть это методы, которые асинхронно выполняют множество действий на странице. Паралельность действий. Например, что-то подгружается с сервера и пользователю не надо перезагружать страничку, оно само появится в нужный момент.
*/



const root = document.getElementById('root')
const films = fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((rsp) => {
        if (rsp.ok) { return rsp.json() }
        throw new Error("Couldn't catch nothing")
    })
    .then(servArr => {
        servArr.forEach((item, index) => {
            let { episodeId: episode, name: title, openingCrawl: opening, characters } = item;
            root.insertAdjacentHTML('beforeend', `    
            <div class="film">
                <h2 class="film-title">TITLE: ${title}</h2>
                <h3 class="film-episode">episode_id: ${episode}</h3>
                <p class="film-opening"><p class="b-text">opening_crawl:</p> ${opening}</p>
                <div class="hero-div" id="${index}"><p class="b-text">characters:</p> </div>
                <div id="${index + 20}" class="lds-spinner">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            `)
            characters.forEach((item, i) => {
                item = fetch(`https://ajax.test-danit.com/api/swapi/people/${i}`)
                    .then((rsp) => {
                        if (rsp.ok) { return rsp.json() }
                        throw new Error("Couldn't catch nothing")
                    })
                    .then(res => {
                        let divHeroes = document.getElementById(`${index + 20}`);
                        let { name } = res;
                        let heroDiv = document.getElementById(`${index}`);
                        let p = document.createElement('p')
                        heroDiv.append(p)
                        p.innerText = (name + ', ')
                        divHeroes.style.display = 'none';
                        p.style.display = 'inline';
                    })
                    .catch((err) => {
                        throw new Error(err)
                    })
            })
        })
    })
    .catch((err) => {
        throw new Error(err)
    })