/*
Асинхронная функция, это функция, которая имеет отдельное место в стеке вызовов. Она возвращает промис. Чтобы показать, что функция асинфхронная, мы перед ней пишем async.
*/

// ____________ Вариант 2 ________________

const buttonToFindIP = document.getElementById('IP');
const divOnPage = document.getElementById("block");

class InformationAboutUser {
    constructor(ip) {
        this.ip = ip;
    }
    async findUserIP() {
        const userIP = await fetch("https://api.ipify.org/?format=json", { method: "GET" })
            .then(response => response.json());
        this.findUserInformationByIP(userIP.ip)
    }
    
    async findUserInformationByIP(ip) {
        const throwUserIP = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district,zip`)
            .then(response => response.json())
        this.render(throwUserIP)
    }
    
    render(userInformation) {
        console.log(userInformation);
        const { continent, country, regionName, city } = userInformation;
        divOnPage.innerHTML = "";
        divOnPage.innerHTML = `
            <p id="continent">Continent: ${continent}</p>
            <p id="country">Country: ${country}</p>
            <p id="regionName">Region Name: ${regionName}</p>
            <p id="city">City: ${city}</p>
        `;
    }
}

const user = new InformationAboutUser;
buttonToFindIP.addEventListener('click', function () {
    console.log('click');
    user.findUserIP();
});