// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
/*
Прототипное наследование это создание нового объекта на основе старого, для дальшего усовершенствования. То есть все функции и методы копируются в другой объект и если мы не можем найти какие то методы или в прототипе, то они берутся с основного.
*/

// CLASS EMPLOYEEE
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name () {
        return this._name;
    }
    set name (name) {
        this._name = name;
    }
    get age () {
        return this._age;
    }
    set age (value) {
        this._age = value;
    }
    get salary () {
        return this._salary;
    }
    set salary (value) {
        this._salary = value;
    }

}
// CLASS PROGRAMMER
class Programmer extends Employee {
    constructor(name, age, _salary, lang) {
        super (name, age, _salary);
        this.lang = lang;
    }
    get salary () {
        return this._salary * 3;
    }
}
// TESTS
let employee1 = new Employee('Ivan', 23, 500);
console.log(employee1);
let employee2 = new Employee('Red', 42, 700);
console.log(employee2);
let programmer1 = new Programmer('Den', 34, 800, ['JS', 'PHP', 'Java']);
console.log(programmer1);
let programmer2 = new Programmer('Carl', 22, 600, ['Python', 'C#']);
console.log(programmer2);