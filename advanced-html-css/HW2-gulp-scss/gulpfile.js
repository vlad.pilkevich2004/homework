const gulp = require('gulp'),
	concat = require('gulp-concat'),
	clean = require('gulp-clean'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	minifyjs = require('gulp-js-minify'),
	imagemin = require('gulp-imagemin'),
	browserSync = require('browser-sync').create(),
	cleanCSS = require('gulp-clean-css'),
	rigger = require('gulp-rigger'),
	autoprefixer = require('gulp-autoprefixer');


/*** PATHS ***/

const paths = {
	src: {
		html: './index.html',
		scss: './src/scss/**/*.scss',
		js: './src/js/*.js',
		img: './src/img/*.png',
	},
	dist: {
		html: './dist/',
		css: './dist/css/',
		js: './dist/js/',
		img: './dist/img/',
		self: './dist/',
	},
}

/*** FUNCTIONS ***/

const buildHTML = () =>
	gulp
		.src(paths.src.html)
		// .pipe(concat('index.html'))
		// .pipe(rigger())  
		.pipe(gulp.dest(paths.dist.html))
		.pipe(browserSync.stream())


const buildJS = () =>
	gulp
		.src(paths.src.js)
		.pipe(concat('script.js'))
		.pipe(uglify())
		.pipe(minifyjs())
		.pipe(gulp.dest(paths.dist.js))
		.pipe(browserSync.stream())


const buildCSS = () =>
	gulp
		.src(paths.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(cleanCSS())
		.pipe(gulp.dest(paths.dist.css))
		.pipe(browserSync.stream())


const buildIMG = () => 
	gulp
		.src(paths.src.img)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.dist.img))
		.pipe(browserSync.stream())


const cleanBuild = () => gulp.src(paths.dist.self, { allowEmpty: true }).pipe(clean())

const watcher = () => {
	browserSync.init({
		server: {
			baseDir: './',
		},
	})
	gulp.watch(paths.src.html, buildHTML).on('change', browserSync.reload)
	gulp.watch(paths.src.scss, buildCSS).on('change', browserSync.reload)
	gulp.watch(paths.src.js, buildJS).on('change', browserSync.reload)
}



/*** TASKS ***/

gulp.task('clean', cleanBuild)
gulp.task('buildHTML', buildHTML)
gulp.task('buildCSS', buildCSS)
gulp.task('buildJS', buildJS)
gulp.task('buildIMG', buildIMG)
gulp.task('watch', watcher);




gulp.task('default', gulp.series(cleanBuild, buildHTML, buildCSS, buildJS, watcher))
gulp.task('build', gulp.series(cleanBuild, gulp.parallel(buildHTML, buildIMG, buildCSS, buildJS)));
gulp.task('dev', gulp.series('build', 'watch'));

