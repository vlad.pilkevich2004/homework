
let burger = document.querySelector('#burger');
let item = document.querySelector('#burgerI');
let burgerX = document.querySelector('#burgerX');

burger.addEventListener('click', function () {
    item.style.display = 'block';
    burger.style.display = 'none';
    burgerX.style.display = 'block';
});

burgerX.addEventListener('click', function () {
    item.style.display = 'none';
    burger.style.display = 'block';
    burgerX.style.display = 'none';
});

window.addEventListener('click', function (e) {
    let top = e.target.closest("#burgerI");
    if (top !== item && e.target !== burger && burgerX.style.display === 'block') {
        item.style.display = 'none';
        burger.style.display = 'block';
        burgerX.style.display = 'none';
    }
})