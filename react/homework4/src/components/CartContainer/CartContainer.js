import React from "react";
import {useSelector} from "react-redux";
import styles from './CartContainer.module.scss';
import CartItem from "../CartItem/CartItem.js";

function CartContainer() {
    const cartData = useSelector((state)=>state.cart.inCart);
    // console.log(cartData);
    return (
        <>
            <section className={styles.cardContainer}>
                {cartData ? cartData.map((item) => <CartItem key={item.id} itemContent={item} />) : <h1>No cart items here</h1> }
            </section>
        </>
    );
}

export default CartContainer;
