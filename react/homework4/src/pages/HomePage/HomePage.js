import React, { useEffect, useState } from "react";
import CardsContainer from "../../components/CardsContainer/CardsContainer.js";

const HomePage = () => {
    return (
        <div className="App">
            <h1>Homework 4</h1>
            <CardsContainer />
        </div>
    );
}

export default HomePage;