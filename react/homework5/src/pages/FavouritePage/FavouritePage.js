import React from "react";
import FavouriteContainer from "../../components/FavouriteContainer/FavouriteContainer.js"

const FavouritePage = () => {
    return (
        <section>
            <h1>Favourite:</h1>
            <FavouriteContainer/>
        </section>
    )
}

export default FavouritePage;