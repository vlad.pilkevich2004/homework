import React, { Component } from "react";
import styles from './Button.module.scss';
// import Modal from "../modal/Modal.js";

class Button extends Component {
    render() {
        // console.log(this);
        const { children, handleClick, backgroundColor } = this.props;
        return ( 
            <>
                <button style={{backgroundColor: backgroundColor}} onClick={handleClick} className={styles.btn}>{children}</button>
            </>
        )
    }
}

export default Button;