import React, { PureComponent } from "react";
import styles from './Modal.module.scss';
import Button from "../button/Button.js";


class Modal extends PureComponent {
    state = {
        isOpenModal: false
    }
    openModal = () => {
        { this.setState({ isOpenModal: true }) }
    }
    closeModal = () => {
        { this.setState({ isOpenModal: false }) }
    }
    deleteModalByBg = (e) => {
        // console.log(e.target);
        // console.log(e.target.classList.contains("modalBg"));
        if (e.target.classList.contains(styles.modalBg)) {
            this.closeModal();
        }
    }

    render() {
        const { isOpenModal } = this.state;
        const { modalTitle, modalBody, modalX, buttonText, modalActions, buttonBgColor, colors } = this.props;
        return (
            <>
                <Button backgroundColor={buttonBgColor} handleClick={this.openModal}>{buttonText}</Button>
                {isOpenModal &&
                    <div onClick={this.deleteModalByBg} className={styles.modalBg}>
                        <div style={{backgroundColor: colors.second}} className={styles.modal}>
                            <div style={{backgroundColor: colors.first}} className={styles.modal__topContainer}>
                                {modalX && <button style={{backgroundColor: colors.first}} className={styles.x} onClick={this.closeModal}>✕</button>}
                                <h2>{modalTitle}</h2>
                            </div>
                            <p className={styles.modal__body}>{modalBody}</p>
                            <div onClick={this.closeModal}>
                                {modalActions}
                            </div>
                        </div>
                    </div>}
            </>
        )
    }
}

export default Modal;
