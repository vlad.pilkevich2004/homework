import React, { Component } from "react";
import Button from "./components/button/Button.js";
import Modal from "./components/modal/Modal.js";
import './App.scss';
import modalStyles from './components/modal/Modal.module.scss';

class App extends Component {
  state = {
    firstModal: {
      title: "Do you want to delete this file?",
      body: "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?",
      X: false,
      buttonBgColor: "#B3382C",
      buttonText: "Delete file (first modal)",
      className: "modal",
      colors: {
        first: "#D44637",
        second: "#E74C3C",
        third: "#B3382C"
      }
    },
    secondModal: {
      title: "Do you want to save this file?",
      body: "You can save it whenever you want.",
      X: true,
      buttonText: "Save file (second modal)",
      buttonBgColor: "green",
      className: "modal",
      colors: {
        first: "#0c831c",
        second: "#0aa31f",
        third: "#025b0e"
      }
    }
  }
  render() {
    const { firstModal: { title: firstTitle, body: firstBody, X: fX, buttonText: firstButtonText, buttonBgColor: firstModalButtonBgColor, colors: colorsModalOne },
      secondModal: { title: secondTitle, body: secondBody, X: sX, buttonText: secondButtonText, buttonBgColor: secondModalButtonBgColor, colors: colorsModalTwo } } = this.state;
    const firstModalActions = <>
      <button style={{ backgroundColor: colorsModalOne.third }} className={modalStyles.modalBtn} onClick={() => { console.log("click action first modal") }}>Ok</button>
      <button style={{ backgroundColor: colorsModalOne.third }} className={modalStyles.modalBtn} onClick={() => { console.log("click action first modal") }}>Cancel</button>
    </>
    const secondModalActions = <>
      <button style={{ backgroundColor: colorsModalTwo.third }} className={modalStyles.modalBtn} onClick={() => { console.log("click action second modal") }}>Cancel</button>
    </>
    return (
      <div className="App">

        <h1>Homework 1</h1>

        <Modal modalTitle={firstTitle} colors={colorsModalOne} modalBody={firstBody} modalX={fX} buttonText={firstButtonText} modalActions={firstModalActions} buttonBgColor={firstModalButtonBgColor} />
        <Modal modalTitle={secondTitle} colors={colorsModalTwo} modalBody={secondBody} modalX={sX} buttonText={secondButtonText} modalActions={secondModalActions} buttonBgColor={secondModalButtonBgColor} />

      </div>
    )
  }
}

export default App;