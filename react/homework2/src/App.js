import React, { Component } from "react";
import './App.scss';
import Button from "./components/button/Button.js";
import Modal from "./components/modal/Modal.js";
import CardsContainer from "./components/cardsContainer/CardsContainer.js";
// import dogImg from "./assets/dogImg.jpg";

class App extends Component {
  state = {
    items: null
  }
  async componentDidMount() {
    const result = await fetch('./data/serverAnswer.json').then(res => res.json());
    this.setState((current) => ({ ...current, items: result }));
  }
  render() {
    const { items } = this.state;
    // console.log(items);
    return (
      <div className="App">

        <h1>Homework 2</h1>

        {/* <img src="./images/dog.jpg" /> */}
        {/* <img src="./images/image1.png" /> */}
        {/* <img src={dogImg} /> */}


        {/* {items && console.log(items)} */}
        {items && <CardsContainer items={items}/>}

      </div>
    )
  }
}

export default App;