import React, { PureComponent } from "react";
import styles from './Modal.module.scss';
import Button from "../button/Button.js";
import PropTypes from 'prop-types';


class Modal extends PureComponent {
    state = {
        isOpenModal: false,
        inCart: false
    }
    openModal = () => {
        { this.setState({ isOpenModal: true }) }
    }
    closeModal = () => {
        { this.setState({ isOpenModal: false }) }
    }
    deleteModalByBg = (e) => {
        if (e.target.classList.contains(styles.modalBg)) {
            this.closeModal();
        }
    }
    componentDidMount = () => {
        const { id } = this.props;
        // const itemsInCart = JSON.parse(localStorage.getItem(`inCart_${id}`));
        // console.log(itemsInCart);
        // if (itemsInCart) {
        //     { this.setState({ inCart: true }) }
        // }
        const itemsInCart = JSON.parse(localStorage.getItem('inCart'));
        // console.log(itemsInCart);
        if (itemsInCart) {
            itemsInCart.forEach(item => {
                if (item === id) {
                    { this.setState({ inCart: true }) }
                }
            })     
        }
    }
    sendInCart = (id) => {
        const itemsInCart = JSON.parse(localStorage.getItem('inCart'));
        console.log(itemsInCart);
        if (itemsInCart) {
            // console.log(!itemsInCart.includes(id));
            if (!itemsInCart.includes(id)) {
                itemsInCart.push(id);
                localStorage.setItem('inCart', JSON.stringify(itemsInCart))
            }
        } else {
            localStorage.setItem('inCart', JSON.stringify([id]))
        }
        { this.setState({ inCart: true }) }
        console.log('Сохранение в корзину покупок');
    }

    render() {
        const { isOpenModal, inCart } = this.state;
        const { id, itemContent, modalTitle, modalBody, modalX, buttonText, buttonBgColor, colors } = this.props;
        // console.log(inCart);
        return (
            <>
                <Button backgroundColor={buttonBgColor} handleClick={this.openModal}>{buttonText}</Button>
                {isOpenModal &&
                    <div onClick={this.deleteModalByBg} className={styles.modalBg}>
                        <div style={{ backgroundColor: colors.second }} className={styles.modal}>
                            <div style={{ backgroundColor: colors.first }} className={styles.modal__topContainer}>
                                {modalX && <button style={{ backgroundColor: colors.first }} className={styles.x} onClick={this.closeModal}>✕</button>}
                                <h2>{modalTitle}</h2>
                            </div>
                            <p className={styles.modal__body}>{modalBody}</p>
                            <Button handleClick={() => { this.closeModal(); this.sendInCart(id); }} backgroundColor="#B3382C" style={styles.modalBtn}>Ok</Button>
                        </div>
                    </div>}
            </>
        )
    }
}

Modal.propTypes = {
    modalTitle: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    modalBody: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    modalX: PropTypes.bool,
    buttonText: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    buttonBgColor: PropTypes.string,
    colors: PropTypes.object,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired
}

Modal.defaultProps = {
    modalBody: "",
    modalX: true,
    buttonText: "Button",
    buttonBgColor: "gray",
    colors: {
        first: "#000",
        second: "grey",
        third: "#000"
    }
}

export default Modal;
