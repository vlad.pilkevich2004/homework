import React, { Component } from "react";
import styles from './Button.module.scss';
// import Modal from "../modal/Modal.js";
import PropTypes from 'prop-types';
import modalStyles from '../modal/Modal.module.scss';

class Button extends Component {
    render() {
        // console.log(this);
        const { children, handleClick, backgroundColor, type, style } = this.props;
        return ( 
            <>
                <button type={type} style={{backgroundColor: backgroundColor}} onClick={handleClick} className={styles.btn + " " + style}>{children}</button>
            </>
        )
    }
}

Button.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    type: PropTypes.oneOf(['submit', 'button']),
    handleClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    style: PropTypes.string
}

Button.defaultProps = {
    type: 'button',
    handleClick: ()=>{},
    backgroundColor: 'grey',
    style: ''
}


export default Button;