import React, { PureComponent } from "react";
import styles from './CardItem.module.scss';
// import Button from "../button/Button.js";
import Modal from "../modal/Modal.js";
// import modalStyles from '../modal/Modal.module.scss';
import PropTypes from 'prop-types';
import { ReactComponent as StarIcon } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";



class CardItem extends PureComponent {
    state = {
        isFavourite: false
    }

    componentDidMount = () => {
        const { itemContent: { id } } = this.props;
        // console.log(id);
        const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
        if (favouriteItems) {
            favouriteItems.forEach(item => {
                // console.log(item, id);
                if (item === id) {
                    this.setState({ isFavourite: true });
                }
            })
        }
    }
    addToFavourite = (id) => {
        const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
        console.log(favouriteItems);
        if (favouriteItems) {
            if (!favouriteItems.includes(id)) {
                favouriteItems.push(id);
                localStorage.setItem('isFavourite', JSON.stringify(favouriteItems))
            }
        } else {
            localStorage.setItem('isFavourite', JSON.stringify([id]))
        }
        { this.setState({ isFavourite: true }) }
    }
    removeFromFavourite = (id) => {
        const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
        console.log(favouriteItems);
        if (favouriteItems) {
            const newFavouriteItems = favouriteItems.map(item => {
                if (item !== id) {
                    return item;
                }
            })
            const filteredFav = newFavouriteItems.filter(Number);
            console.log(newFavouriteItems, favouriteItems, filteredFav);
            localStorage.setItem('isFavourite', JSON.stringify(filteredFav))
        }
        { this.setState({ isFavourite: false }) }
    }
    render() {
        const { isFavourite } = this.state;
        const { itemContent, itemContent: { name, price, url, id, color } } = this.props;
        // console.log(itemContent);
        return (
            <>
                <div class={styles.productItem}>
                    <div class={styles.productImg}>
                        <img src={url} />

                    </div>
                    <div class={styles.productIist}>
                        <h3>{name}</h3>
                        <div class={styles.stars}></div>
                        <span class={styles.price}>{price}</span>
                        <p class={styles.colorContainer}>Cover color: {color} <div class={styles.colors} style={{backgroundColor: color}}></div></p>
                        <div class={styles.actions}>
                            <div class={styles.addToCart}>
                                <Modal id={id} itemContent={itemContent} modalTitle="Add to cart?" modalBody="Are you sure you want to add this item to your shopping cart?" modalX={true}
                                    buttonText="Add to Shopping Cart" buttonBgColor="#424242" colors={{ first: "#D44637", second: "#E74C3C", third: "#B3382C" }} />
                            </div>
                            <div class={styles.addToLinks}>
                                <div className={styles.favourites}>
                                    {isFavourite ? <StarRemove onClick={() => this.removeFromFavourite(id)} /> : <StarIcon onClick={() => this.addToFavourite(id)} />}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

CardItem.propTypes = {
    itemContent: PropTypes.object.isRequired,
    name: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    url: PropTypes.string,
    id: PropTypes.number,
    color: PropTypes.string
}

CardItem.defaultProps = {
    name: '',
    price: 0,
    url: '',
    id: null,
    color: ''
}

export default CardItem;