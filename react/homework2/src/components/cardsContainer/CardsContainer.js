import React, { PureComponent } from "react";
import styles from './CardsContainer.module.scss';
// import Button from "../button/Button.js";
import CardItem from "../cardItem/CardItem.js";
import PropTypes from 'prop-types';


class CardsContainer extends PureComponent {
    state = {
        isOpenModal: false
    }


    render() {
        const { items } = this.props;
        // console.log(items);
        return (
            <>
                <section className={styles.cardContainer}>
                    {items.map((item) => <CardItem key={item.id} itemContent={item} />)}
                </section>
            </>
        )
    }
}

CardsContainer.propTypes = {
    items: PropTypes.array.isRequired
}

export default CardsContainer;
