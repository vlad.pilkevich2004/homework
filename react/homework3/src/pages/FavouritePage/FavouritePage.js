import React, { useEffect, useState } from "react";
import CardsContainer from "../../components/cardsContainer/CardsContainer.js";

const FavouritePage = (props) => {
    const [items, setItems] = useState(null);
    useEffect(() => {
        (async () => {
            const result = await fetch('./data/serverAnswer.json').then(res => res.json());
            setItems(result);
        })()
    }, []);
    const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
    const findItems = () => {
        return items.reduce((acc, i) => {
            if (Array.isArray(favouriteItems)) {
                if (favouriteItems.includes(i.id)) {
                    acc.push(i);
                }
            }
            return acc;
        }, [])
    }
    return (
        <section>
            <h1>Favourite:</h1>
            {items && <CardsContainer items={findItems()} />}
        </section>
    )
}

export default FavouritePage;