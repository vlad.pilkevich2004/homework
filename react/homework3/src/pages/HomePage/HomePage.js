import React, { useEffect, useState } from "react";
import Button from "../../components/button/Button.js";
import Modal from "../../components/modal/Modal.js";
import CardsContainer from "../../components/cardsContainer/CardsContainer.js";

const HomePage = () => {
    const [items, setItems] = useState(null);
    useEffect(() => {
        (async () => {
            const result = await fetch('./data/serverAnswer.json').then(res => res.json());
            setItems(result);
        })()
    }, []);
    return (
        <div className="App">
            <h1>Homework 3</h1>
            {items && <CardsContainer items={items} />}
        </div>
    );
}

export default HomePage;