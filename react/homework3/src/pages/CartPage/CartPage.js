import React, { useEffect, useState } from "react";
import CardsContainer from "../../components/cardsContainer/CardsContainer.js";
import axios from "axios";


const CartPage = () => {
    const [items, setItems] = useState(null);
    useEffect(() => {
        (async () => {
            try {
                const { status, data: result } = await axios('./data/serverAnswer.json');
                if (status === 200) {
                    setItems(result);
                }
            } catch (error) {
                console.error(error)
            }
        })()
    }, []);
    const cartItems = JSON.parse(localStorage.getItem('inCart'));
    const findItems = () => {
        return items.reduce((acc, i) => {
            if (Array.isArray(cartItems)) {
                if (cartItems.includes(i.id)) {
                    acc.push(i);
                }  
            }
            return acc;
        }, [])
    }
    return (
        <section>
            <h1>Cart:</h1>
            {items && <CardsContainer specialFunctions={true} items={findItems()} />}
        </section>
    )
}

export default CartPage;