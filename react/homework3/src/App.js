import React, { Component } from "react";
// import React, { useEffect, useState } from "react";
import './App.scss';
// import Button from "./components/button/Button.js";
// import Modal from "./components/modal/Modal.js";
// import CardsContainer from "./components/cardsContainer/CardsContainer.js";
// -------------------------------------------------------------------------------------------------------------------------------------
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes/Routes';
import Header from "./components/Header/Header.js";
import Footer from "./components/Footer/Footer.js";

function App() {
  return (
    <Router>
      <div className="App">
        <Header/>
        <Routes/>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;