import React, { useEffect, useState } from "react";
import styles from './CardItem.module.scss';
import Modal from "../modal/Modal.js";
import PropTypes from 'prop-types';
import { ReactComponent as StarIcon } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";


function CardItem(props) {
    const [isFavourite, setIsFavourite] = useState(false);
    const { specialFunctions, itemContent, itemContent: { name, price, url, id, color } } = props;
    useEffect(() => {
        const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
        if (favouriteItems) {
            favouriteItems.forEach(item => {
                if (item === id) {
                    setIsFavourite(true);
                }
            })
        }
    }, []);
    const addToFavourite = (id) => {
        const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
        if (favouriteItems) {
            if (!favouriteItems.includes(id)) {
                favouriteItems.push(id);
                localStorage.setItem('isFavourite', JSON.stringify(favouriteItems))
            }
        } else {
            localStorage.setItem('isFavourite', JSON.stringify([id]))
        }
        setIsFavourite(true);
    }
    const removeFromFavourite = (id) => {
        const favouriteItems = JSON.parse(localStorage.getItem('isFavourite'));
        if (favouriteItems) {
            const newFavouriteItems = favouriteItems.map(item => {
                if (item !== id) {
                    return item;
                }
            })
            const filteredFav = newFavouriteItems.filter(Number);
            localStorage.setItem('isFavourite', JSON.stringify(filteredFav));
            if (document.querySelector("h1").textContent === "Favourite:") {
                const itemToRemoveFromFav = document.getElementById(id);
                itemToRemoveFromFav.remove();
            }
        }
        setIsFavourite(false);
    }
    return (
        <>
            <div id={id} class={styles.productItem}>
                <div class={styles.productImg}>
                    <img src={url} />

                </div>
                <div class={styles.productIist}>
                    <h3>{name}</h3>
                    <div class={styles.stars}></div>
                    <span class={styles.price}>{price}</span>
                    <p class={styles.colorContainer}>Cover color: {color} <div class={styles.colors} style={{ backgroundColor: color }}></div></p>
                    <div class={styles.actions}>
                        <div class={styles.addToCart}>
                            {specialFunctions ? 
                            <Modal id={id} specialFunctions={specialFunctions} itemContent={itemContent} modalTitle="Delte from cart?" modalBody="Are you sure you want to delete this item from your shopping cart?" modalX={true}
                            buttonText="Delete from Shopping Cart" buttonBgColor="#424242" colors={{ first: "#D44637", second: "#E74C3C", third: "#B3382C" }} />
                            :
                            <Modal id={id} specialFunctions={specialFunctions} itemContent={itemContent} modalTitle="Add to cart?" modalBody="Are you sure you want to add this item to your shopping cart?" modalX={true}
                                buttonText="Add to Shopping Cart" buttonBgColor="#424242" colors={{ first: "#D44637", second: "#E74C3C", third: "#B3382C" }} />
                            }
                        </div>
                        <div class={styles.addToLinks}>
                            <div className={styles.favourites}>
                                {isFavourite ? <StarRemove onClick={() => removeFromFavourite(id)} /> : <StarIcon onClick={() => addToFavourite(id)} />}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

CardItem.propTypes = {
    itemContent: PropTypes.object.isRequired,
    name: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    url: PropTypes.string,
    id: PropTypes.number,
    color: PropTypes.string,
    specialFunctions: PropTypes.bool
}

CardItem.defaultProps = {
    name: '',
    price: 0,
    url: '',
    id: null,
    color: '',
    specialFunctions: false
}

export default CardItem;