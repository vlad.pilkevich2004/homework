import React, { useEffect, useState } from "react";
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

function Button(props) {
    const { children, handleClick, backgroundColor, type, style } = props;
    return ( 
        <>
            <button type={type} style={{backgroundColor: backgroundColor}} onClick={handleClick} className={styles.btn + " " + style}>{children}</button>
        </>
    )
}

Button.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    type: PropTypes.oneOf(['submit', 'button']),
    handleClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    style: PropTypes.string
}

Button.defaultProps = {
    type: 'button',
    handleClick: ()=>{},
    backgroundColor: 'grey',
    style: ''
}

export default Button;