import React, { useEffect, useState } from "react";
import styles from './Modal.module.scss';
import Button from "../button/Button.js";
import PropTypes from 'prop-types';


function Modal(props) {
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [inCart, setInCart] = useState(false);
    const { id, specialFunctions, itemContent, modalTitle, modalBody, modalX, buttonText, buttonBgColor, colors } = props;
    useEffect(() => {
        const itemsInCart = JSON.parse(localStorage.getItem('inCart'));
        if (itemsInCart) {
            itemsInCart.forEach(item => {
                if (item === id) {
                    setInCart(true);
                }
            })
        }
    });
    const openModal = () => {
        setIsOpenModal(true);
    }
    const closeModal = () => {
        setIsOpenModal(false);
    }
    const deleteModalByBg = (e) => {
        if (e.target.classList.contains(styles.modalBg)) {
            closeModal();
        }
    }
    const sendInCart = () => {
        const itemsInCart = JSON.parse(localStorage.getItem('inCart'));
        if (itemsInCart) {
            if (!itemsInCart.includes(id)) {
                itemsInCart.push(id);
                localStorage.setItem('inCart', JSON.stringify(itemsInCart))
            }
        } else {
            localStorage.setItem('inCart', JSON.stringify([id]))
        }
        setInCart(true);
        console.log('Сохранение в корзину покупок');
    }
    const removeFromCart = (id) => {
        const itemsInCart = JSON.parse(localStorage.getItem('inCart'));
        if (itemsInCart) {
            const newitemsInCart = itemsInCart.map(item => {
                if (item !== id) {
                    return item;
                }
            })
            const filteredCart = newitemsInCart.filter(Number);
            localStorage.setItem('inCart', JSON.stringify(filteredCart));
            const itemToRemoveFromCart = document.getElementById(id);
            itemToRemoveFromCart.remove();
        }
        setInCart(false);
    }
    return (
        <>
            <Button backgroundColor={buttonBgColor} handleClick={openModal}>{buttonText}</Button>
            {isOpenModal &&
                <div onClick={deleteModalByBg} className={styles.modalBg}>
                    <div style={{ backgroundColor: colors.second }} className={styles.modal}>
                        <div style={{ backgroundColor: colors.first }} className={styles.modal__topContainer}>
                            {modalX && <button style={{ backgroundColor: colors.first }} className={styles.x} onClick={closeModal}>✕</button>}
                            <h2>{modalTitle}</h2>
                        </div>
                        <p className={styles.modal__body}>{modalBody}</p>
                        {specialFunctions ? <Button handleClick={() => { closeModal(); removeFromCart(id); }} backgroundColor="#B3382C" style={styles.modalBtn}>Okay</Button> : <Button handleClick={() => { closeModal(); sendInCart(id); }} backgroundColor="#B3382C" style={styles.modalBtn}>Ok</Button>}
                    </div>
                </div>}
        </>
    );
}


Modal.propTypes = {
    modalTitle: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    modalBody: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    modalX: PropTypes.bool,
    buttonText: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    buttonBgColor: PropTypes.string,
    colors: PropTypes.object,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    specialFunctions: PropTypes.bool
}

Modal.defaultProps = {
    modalBody: "",
    modalX: true,
    buttonText: "Button",
    buttonBgColor: "gray",
    colors: {
        first: "#000",
        second: "grey",
        third: "#000"
    },
    specialFunctions: false
}

export default Modal;
