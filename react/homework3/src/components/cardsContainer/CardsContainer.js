// import React, { PureComponent } from "react";
import React, { useEffect, useState } from "react";
import styles from './CardsContainer.module.scss';
import CardItem from "../cardItem/CardItem.js";
import PropTypes from 'prop-types';

function CardsContainer(props) {
    const [isOpenModal, setIsOpenModal] = useState(false);
    const { specialFunctions, items } = props;
    return (
        <>
            <section className={styles.cardContainer}>
                {Object.keys(items).length == 0 && <h1>No items here</h1>}
                {items.map((item) => <CardItem key={item.id} specialFunctions={specialFunctions} itemContent={item} />)}
            </section>
        </>
    );
}

CardsContainer.propTypes = {
    items: PropTypes.array.isRequired,
    specialFunctions: PropTypes.bool
}

CardsContainer.defaultProps = {
    specialFunctions: false
}

export default CardsContainer;
